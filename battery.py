import math
from kivy.uix.widget import Widget

class Battery(Widget):

    soc = 0 # state of charge expressed as a fraction between 0 and 1
    lut = None
    reference_voltage = None # reference_voltage is the maximum voltage that can be emulated
    mAh_capacity = None # industry-common capacity rating
    joule_capacity = None # actual integrated capcacity based on mAh capacity

    def __init__(self, reference_voltage, lut, mAh):
        self.reference_voltage = reference_voltage
        self.set_lut(lut)
        self.set_capacity(mAh)

    def set_lut(self, lut):
        self.lut = lut
        self.apply_voltage_limits()

    def get_lut(self):
        return self.lut

    def set_capacity(self, mAh):

        if mAh <= 0:
            raise ValueError("Battery capacity must be greater than zero.")

        PERIOD = 3600 # seconds in an hour
        INTERVAL = 1 # resolution of discrete integration
        discharge_rate = 1.0/PERIOD # fractional SoC decrease per second
        discharge_current = mAh/1000.0

        soc = 1.0
        joules = 0.0
        for step in range(math.floor(PERIOD/INTERVAL)):

            instantaneous_power = self.lut.lookup(soc)*discharge_current
            joules = joules + instantaneous_power*INTERVAL
            soc = soc - discharge_rate*INTERVAL;

        self.mAh_capacity = mAh
        if (self.joule_capacity != None):
            self.set_soc(self.soc*self.joule_capacity/joules)
        self.joule_capacity = joules

    def get_capacity(self):
        return self.mAh_capacity

    def set_soc(self, soc):
        self.soc = soc
        self.apply_voltage_limits()

    def set_voltage(self, V):
        self.soc = self.lut.reverse_lookup(V)
        self.apply_voltage_limits()

    def get_soc(self):
        return self.soc

    def get_voltage(self):
        return self.lut.lookup(self.soc)

    def set_reference_voltage(self, V):
        self.reference_voltage = V
        self.apply_voltage_limits()

    def get_max_soc(self):
        return self.lut.reverse_lookup(self.reference_voltage)

    def get_reference_voltage(self):
        return self.reference_voltage

    def add_joules(self, joules):
        self.soc = self.soc + float(joules)/self.joule_capacity
        self.apply_voltage_limits()

    def apply_voltage_limits(self):
        if self.get_voltage() > self.reference_voltage:
            self.set_soc(self.get_max_soc())
        elif self.get_voltage() < 0:
            self.set_soc(0)
