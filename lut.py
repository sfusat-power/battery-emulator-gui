from kivy_garden.graph import MeshLinePlot

class LUT:

    def __init__(self):
        self.empty = True

    def __init__(self, inputs, outputs):
        self.inputs = inputs
        self.outputs = outputs

        if len(inputs) != len(outputs):
            raise ValueError("LUT dimensions mismatch")

        if len(inputs) == 0:
            raise ValueError("LUT is empty")

        if len(inputs) == 1:
            self.end_rate = 0
            self.start_rate = 0
        else:
            # Check to make sure LUT inputs are in order and 1:1 with outputs
            for i in range(len(inputs) - 1):
                if inputs[i + 1] - inputs[i] == 0:
                        raise ZeroDivisionError("LUT has multiple outputs for same input")
                if inputs[i + 1] < inputs[i] == 0:
                        raise ValueError("LUT inputs must be sorted to monotonically increase")

            self.end_rate = (outputs[-1] - outputs[-2])/(inputs[-1] - inputs[-2])
            self.start_rate = (outputs[1] - outputs[0])/(inputs[1] - inputs[0])

        self.empty = False

    def lookup(self, x):
        if self.empty:
            raise RuntimeError("Cannot lookup from empty LUT")

        if x < self.inputs[0]:
            return self.outputs[0] - (self.inputs[0] - x)*self.start_rate
        elif x > self.inputs[-1]:
            return self.outputs[-1] + (x - self.inputs[-1])*self.end_rate
        else:
            for i in range(len(self.inputs) - 1):
                if x >= self.inputs[i] and x <= self.inputs[i+1]:
                    rate = (self.outputs[i+1] - self.outputs[i])/(self.inputs[i+1] - self.inputs[i])
                    return self.outputs[i] + (x - self.inputs[i])*rate

    def reverse_lookup(self, y):
        if self.empty:
            raise RuntimeError("Cannot lookup from empty LUT")

        if y < self.outputs[0]:
            return self.inputs[0] - (self.outputs[0] - y)*(1/self.start_rate)
        elif y > self.outputs[-1]:
            return self.inputs[-1] + (y - self.outputs[-1])*(1/self.end_rate)
        else:
            for i in range(len(self.outputs) - 1):
                if y >= self.outputs[i] and y <= self.outputs[i+1]:
                    rate = (self.inputs[i+1] - self.inputs[i])/(self.outputs[i+1] - self.outputs[i])
                    return self.inputs[i] + (y - self.outputs[i])*rate

    def get_plot(self):
        N = 50
        step_size = float(self.inputs[-1] - self.inputs[0])/N
        samples = [step_size*i for i in range(N+1)]
        line_plot = MeshLinePlot(color=[0, 1, 0, 1])
        line_plot.points = [(x*100, self.lookup(x)) for x in samples]
        return line_plot