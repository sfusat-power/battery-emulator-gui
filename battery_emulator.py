from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.pagelayout import PageLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.slider import Slider
from kivy.clock import Clock
import sys
import glob
import serial

# project files
from lut import LUT
from battery import Battery
from dropdownmenu import DropDownMenu
from cursorgraph import CursorGraph

IDLE = 0
MANUAL = 1
INTEGRATING = 2
SWEEP_ONESHOT = 3
SWEEP_REPEATED = 4
SEQUENCE = 5

class BatteryEmulatorApp(App):

    def __init__(self, **kwargs):
        super(BatteryEmulatorApp, self).__init__(**kwargs)
        self.voltage_adc_value = 0
        self.serial_port = None
        self.battery = Battery(4.4, LUT([0, 0.1, 0.9, 1.0], [0, 3.6, 3.8, 4.2]), 3200) # these are initial settings  that cna be changed later

    # Returns list of the serial ports available on the system
    def get_serial_ports(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def open_serial_port(self, line):
        if self.serial_port:
            self.serial_port.close()
        try:
            self.serial_port = serial.Serial(port=line, baudrate=115200)
            self.serial_menu.text = "Serial: " + line
        except:
            self.serial_port = None
            self.serial_menu.text = "Serial: None"
            self.transition(IDLE)

    def refresh_widget_values(self):
        self.soc_slider.value = self.battery.get_soc()*100
        self.soc_numeric.text = "{:.4f}".format(self.battery.get_soc()*100)
        self.voltage_slider.value = self.battery.get_voltage()
        self.voltage_numeric.text = "{:.4f}".format(self.battery.get_voltage())
        self.reference_numeric.text = "{:.4f}".format(self.battery.get_reference_voltage())
        self.capacity_numeric.text = "{:.4f}".format(self.battery.get_capacity())
        self.graph.set_cursor_position(self.battery.get_soc()*100, self.battery.get_voltage())
        self.refresh_sensor_widget_values()

    def refresh_sensor_widget_values(self):
        self.voltage_adc_label.text = "{:.4f}".format(self.get_sensor_voltage())

    def refresh_widget_bounds(self):
        self.soc_slider.max = self.battery.get_max_soc()*100
        self.voltage_slider.max = self.battery.get_reference_voltage()
        self.graph.xmax = self.battery.get_max_soc()*100
        self.graph.ymax = self.battery.get_reference_voltage()

    def set_soc_slider(self, *args):
        try:
            self.battery.set_soc(self.soc_slider.value/100.0)
        except:
            pass
        self.refresh_widget_values()

    def set_soc_numeric(self, *args):
        try:
            self.battery.set_soc(float(self.soc_numeric.text.strip(" \r\n\t"))/100.0)
        except:
            pass
        self.refresh_widget_values()

    def set_voltage_slider(self, *args):
        try:
            self.battery.set_voltage(self.voltage_slider.value)
        except:
            pass
        self.refresh_widget_values()

    def set_voltage_numeric(self, *args):
        try:
            self.battery.set_voltage(float(self.voltage_numeric.text.strip(" \r\n\t")))
        except:
            pass
        self.refresh_widget_values()

    def set_reference_numeric(self, *args):
        try:
            self.battery.set_reference_voltage(float(self.reference_numeric.text.strip(" \r\n\t")))
        except:
            pass
        self.refresh_widget_values()
        self.refresh_widget_bounds()

    def set_capacity_numeric(self, *args):
        try:
            self.battery.set_capacity(float(self.capacity_numeric.text.strip(" \r\n\t")))
        except:
            pass
        self.refresh_widget_values()

    def refresh_sensor_values(self):

        serial_bytes = self.serial_port.read_until()

        # encode a 16-bit integer from received data
        self.voltage_adc_value = int.from_bytes(serial_bytes[:2], byteorder="big")

        self.refresh_sensor_widget_values()

    def get_sensor_voltage(self):
        # recenter possible range from midscale to 0, divide by half-range to get fractional voltage, and multiply by vref
        return self.battery.get_reference_voltage()*(self.voltage_adc_value - 32768)/(65535 - 32768);

    # state transition safety checks and state setup
    # TODO: grey-out labels when not updating
    def transition(self, state):

        if state == IDLE:
            self.soc_slider.disabled = True
            self.soc_numeric.disabled = True
            self.voltage_slider.disabled = True
            self.voltage_numeric.disabled = True
            self.reference_numeric.disabled = True
            self.capacity_numeric.disabled = True
            self.state = IDLE

        elif state == MANUAL:
            self.soc_slider.disabled = False
            self.soc_numeric.disabled = False
            self.voltage_slider.disabled = False
            self.voltage_numeric.disabled = False
            self.reference_numeric.disabled = False
            self.capacity_numeric.disabled = False
            self.state = MANUAL

        elif state == INTEGRATING:
            pass
        elif state == SWEEP_ONESHOT:
            pass
        elif state == SWEEP_REPEATED:
            pass
        elif state == SEQUENCE:
            pass
        else:
            raise ValueError("{0} is not a valid state.".format(state))

    # state operations
    def update(self, dt):

        if self.state == IDLE:
            if self.serial_port:
                self.transition(MANUAL)

        elif self.state == MANUAL:
            self.refresh_sensor_values()

        elif self.state == INTEGRATING:
            pass
        elif self.state == SWEEP_ONESHOT:
            pass
        elif self.state == SWEEP_REPEATED:
            pass
        elif self.state == SEQUENCE:
            pass
        else:
            raise ValueError("{0} is not a valid state.".format(self.state))

    def build(self):

        root = BoxLayout(orientation="horizontal")

        # Graph and sliders arrange in a grid
        graph_layout = GridLayout(cols=2)

        self.graph = CursorGraph(self.battery.get_lut().get_plot(), xlabel="SoC", ylabel="Cell Voltage", x_ticks_minor=10,
                                x_ticks_major=10, y_ticks_major=0.25, y_ticks_minor = 5, y_grid_label=True,
                                x_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=0, ymin=0,
                                size_hint=(0.9, 0.9))

        self.graph.add_asymptote([0,4.2], [1,0], [1, 0, 0, 0.7])
        self.graph.add_asymptote([100,0], [0, 1], [1, 1, 1, 0.7])
        graph_layout.add_widget(self.graph)

        self.voltage_slider = Slider(orientation="vertical", value_track=True, value_track_color=[0, 1, 0, 1], min=0,max=105,value=100,size_hint=(0.1,0.9))
        self.voltage_slider.bind(value=self.set_voltage_slider)
        graph_layout.add_widget(self.voltage_slider)

        self.soc_slider = Slider(orientation="horizontal", value_track=True, value_track_color=[0, 1, 0, 1], min=0,max=105,value=100,size_hint=(0.9,0.1))
        self.soc_slider.bind(value=self.set_soc_slider)
        graph_layout.add_widget(self.soc_slider)

        root.add_widget(graph_layout)

        # Readouts and buttons arranged in a vertical section
        control_panel_layout = BoxLayout(orientation="vertical", size_hint=(.3, 1))

        self.serial_menu = DropDownMenu(self.get_serial_ports(), self.open_serial_port, text='Serial: None', size_hint=(1.0, None), height=44)
        control_panel_layout.add_widget(self.serial_menu)

        numeric_gauge_layout = GridLayout(cols=2, size_hint=(1.0, 0.5))

        numeric_gauge_layout.add_widget(Label(text="Capacity (mAh): "))
        self.capacity_numeric = TextInput(multiline=False)
        self.capacity_numeric.bind(on_text_validate=self.set_capacity_numeric)
        numeric_gauge_layout.add_widget(self.capacity_numeric)

        numeric_gauge_layout.add_widget(Label(text="REF Voltage: "))
        self.reference_numeric = TextInput(multiline=False)
        self.reference_numeric.bind(on_text_validate=self.set_reference_numeric)
        numeric_gauge_layout.add_widget(self.reference_numeric)

        numeric_gauge_layout.add_widget(Label(text="SoC (%): "))
        self.soc_numeric = TextInput(multiline=False)
        self.soc_numeric.bind(on_text_validate=self.set_soc_numeric)
        numeric_gauge_layout.add_widget(self.soc_numeric)

        numeric_gauge_layout.add_widget(Label(text="DAC Voltage: "))
        self.voltage_numeric = TextInput(multiline=False)
        self.voltage_numeric.bind(on_text_validate=self.set_voltage_numeric)
        numeric_gauge_layout.add_widget(self.voltage_numeric)

        numeric_gauge_layout.add_widget(Label(text="ADC Voltage: "))
        self.voltage_adc_label = Label(halign="left")
        numeric_gauge_layout.add_widget(self.voltage_adc_label)

        numeric_gauge_layout.add_widget(Label(text="ADC Current: "))
        self.current_display = Label(halign="left")
        numeric_gauge_layout.add_widget(self.current_display)

        control_panel_layout.add_widget(numeric_gauge_layout)

        control_panel_layout.add_widget(Label(text="PLACEHOLDER"))

        root.add_widget(control_panel_layout)

        # Adjust all widgets for starting conditions
        self.refresh_widget_values()
        self.refresh_widget_bounds()

        # Set initial state
        self.transition(IDLE)

        # Schedule events
        Clock.schedule_interval(self.update, 0.1)

        return root

if __name__ == "__main__":
    BatteryEmulatorApp().run()