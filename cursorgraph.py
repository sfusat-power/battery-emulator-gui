from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse
from kivy.metrics import dp
from kivy_garden.graph import Graph, MeshLinePlot

class Cursor(Widget):
    def __init__(self, **kwargs):
        super(Cursor, self).__init__(**kwargs)
        self.draw()
        self.bind(pos=self.redraw, size=self.redraw)

    def draw(self):
        with self.canvas:
            Color(0, 1, 0, 1)
            self.ellipse = Ellipse(width=dp(2))

    def redraw(self, *args):
        # reuse
        self.ellipse.pos = (self.pos[0] - self.size[0]/2, self.pos[1] - self.size[1]/2)
        self.ellipse.size = self.size

class CursorGraph(Graph):

    def __init__(self, cursor_plot, **kwargs):
        super(CursorGraph, self).__init__(**kwargs)
        self.cursor_plot = cursor_plot
        self.add_plot(self.cursor_plot)
        self.cursor = Cursor(size=[14, 14])
        self.add_widget(self.cursor)

    def add_asymptote(self, intercept, direction, color):
        line_plot = MeshLinePlot(color=color)
        length = 999999
        point_a = (intercept[0] - direction[0]*length/2, intercept[1] - direction[1]*length/2)
        point_b = (intercept[0] + direction[0]*length/2, intercept[1] + direction[1]*length/2)
        line_plot.points = [point_a, point_b]
        self.add_plot(line_plot)

    # set the cursor position in the plot coordinate frame
    def set_cursor_position(self, x, y):
        self.cursor.pos=(self.pos[0] + self.cursor_plot.x_px()(x),self.pos[1] + self.cursor_plot.y_px()(y))